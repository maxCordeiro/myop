using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class GalleryBtnEntity : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public LevelBuilder levelBuilder;
    
    public void OnPointerEnter(PointerEventData eventData)
    {
        //levelBuilder.uiGalleryTilesText.text = "Entities <c=ffffff55>| " + tile.tileID.ToString() + "</c>";
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        levelBuilder.uiGalleryTilesText.text = "Entities";
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
