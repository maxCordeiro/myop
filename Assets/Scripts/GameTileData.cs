using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Xml.Serialization;

[Serializable]
public class GameTileData
{
    [XmlAttribute("id")]
    public TileType id;

    [XmlElement("sound_step")]
    public SoundType soundStep;

    [XmlElement("canBeMoved")]
    public bool canBeMoved;
    // logic

    public GameTileData()
    {

    }
}
