﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Tilemaps;

public class TileBuilderDeprecated : MonoBehaviour
{
    public static TileBuilderDeprecated instance;
    public Tilemap tilemap;

    public Dictionary<Vector3, GameTile> tiles;

    public TileBase selectedTile;
    public GameTile _tile;

    Vector3 playerPos = new Vector3(0f, 0.375f, 0);

    bool withinBounds;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        } else if (instance != this)
        {
            Destroy(gameObject);
        }

        GetWorldTiles();
    }

    public void GetWorldTiles()
    {
        tiles = new Dictionary<Vector3, GameTile>();

        foreach (Vector3Int pos in tilemap.cellBounds.allPositionsWithin)
        {
            var _localPos = new Vector3Int(pos.x, pos.y, pos.z);

            if (!tilemap.HasTile(_localPos)) continue;

            var tile = new GameTile
            {
                localPos = _localPos,
                worldPos = tilemap.CellToWorld(_localPos),
                tileBase = selectedTile,
                tilemapMember = tilemap,
                //tileID = _localPos.x + ", " + _localPos.y
                
            };

            tiles.Add(tile.worldPos, tile);
        }
    }

    private void Update()
    {
        if (EventSystem.current.IsPointerOverGameObject()) return;

        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector3Int currentCell = tilemap.WorldToCell(new Vector3(mousePos.x, mousePos.y, 0));

        Debug.Log(currentCell);

        withinBounds = (currentCell.x > -15 && currentCell.x < 20) && (currentCell.y > -13 && currentCell.y < 11);

        if (!withinBounds) return;

        if (Input.GetMouseButton(0))
        {
            if (!tiles.TryGetValue(currentCell, out _tile))
            {
                tilemap.SetTile(currentCell, selectedTile);
                tilemap.RefreshAllTiles();
            }
        } else if (Input.GetMouseButton(1))
        {
            if (tilemap.GetTile(currentCell))
            {
                tilemap.SetTile(currentCell, null);
                tilemap.RefreshAllTiles();
            }
        }
    }

    public void SaveTiles()
    {
        GetWorldTiles();
    }

    public void LoadTiles()
    {
        foreach (Vector3Int pos in tilemap.cellBounds.allPositionsWithin)
        {
            var _localPos = new Vector3Int(pos.x, pos.y, pos.z);
            var _worldPos = tilemap.CellToWorld(_localPos);

            if (tiles.ContainsKey(_worldPos))
            {
                Debug.Log("Tile found: " + _worldPos.x + ", " + _worldPos.y);
                tilemap.SetTile(_localPos, tiles[_worldPos].tileBase);
            } else
            {
                tilemap.SetTile(_localPos, null);
            }
        }
    }

    public void PlayGame()
    {
        EntityPlayer p = GameObject.FindObjectOfType<EntityPlayer>();

        //p.transform.position = playerPos;
        p.canMove = true;
        p.isGravityOn = true;
    }
}
