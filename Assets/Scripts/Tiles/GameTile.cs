﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class GameTile
{
    public Vector3Int localPos;
    public Vector3 worldPos;

    public TileBase tileBase;
    public Tilemap tilemapMember;

    //----------------------------

    public TileType tileID;


}