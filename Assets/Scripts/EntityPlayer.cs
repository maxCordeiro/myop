﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public class EntityPlayer : Entity
{
    public Vector2 input;
    public float jumpHeight;

    public Entity heldEntity;

    // Update is called once per frame
    public override void Update()
    {
        ResetVelocityY();

        if (charCont.isGrounded && rPlayer.GetButtonDown("A"))
        {
            velocity.y = Mathf.Sqrt(2f * jumpHeight * -gravity);
        }

        velocity.x = rPlayer.GetAxis("Horizontal");

        FlipSprite();
        Move();
        CheckInteraction();
        CheckCeiling();

        if (heldEntity != null)
        {
            heldEntity.isGravityOn = false;
            heldEntity.velocity.y = 0;
            heldEntity.transform.position = transform.position + new Vector3(0f, 1f);
            heldEntity.spRender.flipX = spRender.flipX;

            if (rPlayer.GetButtonUp("X"))
            {
                heldEntity.isGravityOn = true;
                heldEntity = null;
            }
        }
    }

    public void CheckCeiling()
    {
        if (charCont.collisionState.above && !charCont.isGrounded)
        {
            velocity.y = 0;
        }
    }

    public void CheckInteraction()
    {
        var isGoingRight = velocity.x > 0;
        var rayDirection = isGoingRight ? Vector2.right : -Vector2.right;
        var initialRayOrigin = isGoingRight ? charCont._raycastOrigins.bottomRight : charCont._raycastOrigins.bottomLeft;

        RaycastHit2D hit2D = Physics2D.Raycast(initialRayOrigin + new Vector3(0f, 0.25f), rayDirection, 0.2f);
        Debug.DrawRay(initialRayOrigin + new Vector3(0f, 0.25f), rayDirection * 0.2f, Color.green);

        if (hit2D)
        {
            if (hit2D.collider.tag == "Entity")
            {
                Entity e = hit2D.collider.GetComponent<Entity>();
                if (rPlayer.GetButton("X"))
                {
                    heldEntity = e;
                }
            }

            //Debug.Log("Interact: " + hit2D.collider.name);
            
        }
    }

}
