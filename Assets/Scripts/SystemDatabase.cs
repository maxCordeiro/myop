﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;

public class SystemDatabase
{
    public static void LoadDatabase()
    {
        LocalGameDatabase db = new LocalGameDatabase();

        TextAsset dbxml = Resources.Load<TextAsset>("db");
        db = LocalGameDatabase.LoadFromText(dbxml.text);

        DatabaseGameTiles.dbGameTiles = db.dbGameTiles;
    }
}

[XmlRoot("Database")]
public class LocalGameDatabase
{
    [XmlArray("GameTiles")]
    [XmlArrayItem("GameTileData")]
    public List<GameTileData> dbGameTiles;

    public static LocalGameDatabase Load(string path)
    {
        var serializer = new XmlSerializer(typeof(LocalGameDatabase));
        using (var stream = new FileStream(path, FileMode.Open))
        {
            return serializer.Deserialize(stream) as LocalGameDatabase;
        }
    }

    //Loads the xml directly from the given string. Useful in combination with www.text.
    public static LocalGameDatabase LoadFromText(string text)
    {
        var serializer = new XmlSerializer(typeof(LocalGameDatabase));
        return serializer.Deserialize(new StringReader(text)) as LocalGameDatabase;
    }
}