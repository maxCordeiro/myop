using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Title : MonoBehaviour
{
    public Text titleText;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(TitleRoutine());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public IEnumerator TitleRoutine()
    {
        titleText.text = "LOADING DATABASE";

        SystemDatabase.LoadDatabase();

        yield return new WaitForFixedUpdate();

        SceneManager.LoadScene(0);
    }
}
