using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Entity", menuName = "Platformer/Entity")]
public class EntityData : ScriptableObject
{
    public EntityType id;

    public float moveSpeed = 4;
    public bool canBeHeld;

    public float gravityModifier = 1;
}
public enum EntityType
{
    PLAYER = 0,
    SHELL = 1,
}