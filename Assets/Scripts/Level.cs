﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Xml.Serialization;

[Serializable]
public class Level
{
    [XmlElement("level_title")]
    public string levelTitle;

    [XmlElement("level_desc")]
    public string levelDesc;

    [XmlElement("tiles_ground")]
    public Dictionary<Vector3, GameTile> tilesGround;

    [XmlElement("tiles_ground")]
    public Dictionary<Vector3, Entity> entities;

    // music

    // goal

    // difficulty

    // time
}