﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Xml.Serialization;

public class SystemHelper : MonoBehaviour
{
    public static string ToXML(object obj)
    {
        using (var stringwriter = new StringWriter())
        {
            var serializer = new XmlSerializer(obj.GetType());
            serializer.Serialize(stringwriter, obj);
            return stringwriter.ToString();
        }
    }
}