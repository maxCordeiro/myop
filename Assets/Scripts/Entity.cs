﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Prime31;
using Rewired;

public class Entity : MonoBehaviour
{
    public CharacterController2D charCont;
    public Player rPlayer;
    public SpriteRenderer spRender;

    public float gravity;
    public Vector2 velocity;

    public bool canMove;
    public bool isGravityOn;

    public EntityData entityData;

    // Start is called before the first frame update
    public virtual void Start()
    {
        rPlayer = ReInput.players.GetPlayer(0);
        charCont = GetComponent<CharacterController2D>();
        spRender = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    public virtual void Update()
    {

        ResetVelocityY();
        FlipSprite();
        Move();
    }

    public void ResetVelocityY()
    {
        if (charCont.isGrounded)
        {
            velocity.y = 0;
        }
    }


    public void FlipSprite()
    {
        spRender.flipX = velocity.x != 0 ? velocity.x > 0 ? true : false : spRender.flipX;
    }

    public void Move()
    {
        if (canMove)
        {
            if (isGravityOn) velocity.y += (gravity * entityData.gravityModifier) * Time.deltaTime;
            charCont.move(velocity * entityData.moveSpeed * Time.deltaTime);
        }
        
    }
}
