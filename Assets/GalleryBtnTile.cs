using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GalleryBtnTile : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public LevelBuilder levelBuilder;
    public TileType tileID;
    public Button button;

    private void Awake()
    {
        button = GetComponent<Button>();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        levelBuilder.uiGalleryTilesText.text = "Tiles <c=ffffff55>| " + tileID.ToString() + "</c>";
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        levelBuilder.uiGalleryTilesText.text = "Tiles";
    }

    public void Refresh()
    {
        button.transform.GetChild(0).GetComponent<Image>().sprite = Resources.LoadAll<Sprite>("Tilesets/TILE_" + tileID.ToString())[24];
    }
}
