using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Tilemaps;
using UnityEngine.EventSystems;

public class LevelBuilder : MonoBehaviour
{
    public static LevelBuilder instance;
    public Tilemap tilemapGround;

    public Level currentLevel;

    public TileBase selectedTile;
    public GameTile _tile;

    //------------------
    // UI
    public GameObject uiGallery;

    public GameObject uiGalleryTilesParent;
    public SuperTextMesh uiGalleryTilesText;

    public GameObject uiGalleryEntitiesParent;
    public SuperTextMesh uiGalleryEntitiesText;

    public Button uiGalleryButton;

    public Image uiTilePreview;

    public bool inGallery;

    //------------------

    bool withinBounds;

    public EntityPlayer p;

    public Vector2 mouseTile;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }
    
    public void GetGameTiles()
    {
        currentLevel.tilesGround = new Dictionary<Vector3, GameTile>();

        foreach (Vector3Int pos in tilemapGround.cellBounds.allPositionsWithin)
        {
            var _localPos = new Vector3Int(pos.x, pos.y, pos.z);

            if (!tilemapGround.HasTile(_localPos)) continue;

            var tile = new GameTile
            {
                localPos = _localPos,
                worldPos = tilemapGround.CellToWorld(_localPos),
                tileBase = selectedTile,
                tilemapMember = tilemapGround,
                //tileName = _localPos.x + ", " + _localPos.y

            };

            currentLevel.tilesGround.Add(tile.worldPos, tile);
        }
    }


    // Start is called before the first frame update
    void Start()
    {
        GetGameTiles();
        p = GameObject.Find("Player").GetComponent<EntityPlayer>();
        //uiTilePreview.sprite = tilemapGround.

        currentLevel = new Level();
        LoadButtons();
    }

    public void LoadButtons()
    {
        foreach (GameTileData tileData in DatabaseGameTiles.dbGameTiles)
        {
            Debug.Log(tileData);

            GalleryBtnTile buttonTile = Instantiate<GameObject>(Resources.Load<GameObject>("LevelBuilder/ButtonTile"), uiGalleryTilesParent.transform).GetComponent<GalleryBtnTile>();
            buttonTile.tileID = tileData.id;
            buttonTile.levelBuilder = instance;
            buttonTile.Refresh();
        }
    }


    public void OpenGallery()
    {
        inGallery = !inGallery;
        uiGallery.SetActive(inGallery);
    }

    // Update is called once per frame
    void Update()
    {
        if (EventSystem.current.IsPointerOverGameObject()) return;

        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector3Int currentCell = tilemapGround.WorldToCell(new Vector3(mousePos.x, mousePos.y, 0));

        //Debug.Log(currentCell);

        withinBounds = (currentCell.x >= 0 && currentCell.x <= 33) && (currentCell.y >= 0 && currentCell.y <= 22);

        if (!withinBounds) return;

        uiTilePreview.rectTransform.anchoredPosition = new Vector2(currentCell.x, currentCell.y) * 8;

        if (Input.GetMouseButton(0))
        {
            if (!currentLevel.tilesGround.TryGetValue(currentCell, out _tile))
            {
                tilemapGround.SetTile(currentCell, selectedTile);
                tilemapGround.RefreshAllTiles();
            }
        }
        else if (Input.GetMouseButton(1))
        {
            if (tilemapGround.GetTile(currentCell))
            {
                tilemapGround.SetTile(currentCell, null);
                tilemapGround.RefreshAllTiles();
            }
        } else if (Input.GetMouseButton(2))
        {
            p.transform.position = new Vector3(-6.75f, -5.625f, 0) + (new Vector3(currentCell.x * 0.5f, currentCell.y * 0.5f));
        }

    }

    public GameTile Below(GameTile _tile)
    {
        return null;
    }

}